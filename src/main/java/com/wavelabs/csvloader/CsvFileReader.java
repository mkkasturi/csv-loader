package com.wavelabs.csvloader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CsvFileReader {
	int batchsize;
	File file;
	BufferedReader br;

	CsvFileReader(File file, int batchsize) throws FileNotFoundException {
		this.batchsize = batchsize;
		br = new BufferedReader(new FileReader(file));
	}

	public Batch readBatch() throws IOException {
		Batch batch = new Batch();
		String line = "";

		int count = 0;
		while ((line = br.readLine()) != null) {
			batch.setNumOfRecords(batchsize);
			batch.add(line);
			count++;
			if (count == batchsize) {
				break;
			}
		}

		return batch;
	}
}
