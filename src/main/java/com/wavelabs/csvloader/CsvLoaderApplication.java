package com.wavelabs.csvloader;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CsvLoaderApplication {

	public static void main(String[] args) throws IOException {

		CsvFileReader reader = new CsvFileReader(new File("/e:/movie.csv"), 100);
		ExecutorService executor = Executors.newFixedThreadPool(10);
		Batch batch = null;
		long processedRecords = 0;
		while ((batch = reader.readBatch()) != null) {
			if (!batch.empty()) {
				Runnable worker = new Worker(batch, processedRecords);
				executor.execute(worker);
			}
		}
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
	}
}
