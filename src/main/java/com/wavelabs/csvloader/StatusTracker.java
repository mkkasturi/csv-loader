package com.wavelabs.csvloader;

public class StatusTracker {
	private long totalRecords;
	private static long processedRecords;

	public synchronized void recordProcessedCount(long newRecordsProcessed) {
		processedRecords += newRecordsProcessed;
		System.out.println("Processed Records: "+processedRecords);
	}
}
