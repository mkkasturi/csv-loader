package com.wavelabs.csvloader;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class Worker implements Runnable {

	private Batch batch;
	private long processedRecords;

	public Worker(Batch batch, long processedRecords) {
		this.batch = batch;
		this.processedRecords = processedRecords;
	}

	StatusTracker tracker = new StatusTracker();

	PreparedStatement stmt = null;
	static Connection connection = null;
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/imdb", "root", "root");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		String cvsSplitBy = ",";

		ArrayList<String> records = batch.getRecords();
		for (String record : records) {

			String[] details = record.split(cvsSplitBy);
			String movieName = details[0];
			int yearOfTheMovie = Integer.parseInt(details[1]);
			try {
				stmt = (PreparedStatement) connection.prepareStatement("insert into movie values(?,?)");
				stmt.setString(1, movieName);
				stmt.setInt(2, yearOfTheMovie);
				stmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		markComplete();
		/*
		 * try { connection.close(); } catch (SQLException e) {
		 * e.printStackTrace(); }
		 */
	}

	private void markComplete() {
		processedRecords += batch.numOfRecords;
		tracker.recordProcessedCount(processedRecords);
	}
}