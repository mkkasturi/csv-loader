package com.wavelabs.csvloader;

import java.util.ArrayList;

public class Batch {
	int numOfRecords;
	ArrayList<String> records;

	public Batch() {
		records = new ArrayList<String>();
	}

	public int getNumOfRecords() {
		return numOfRecords;
	}

	public void setNumOfRecords(int numOfRecords) {
		this.numOfRecords = numOfRecords;
	}

	public ArrayList<String> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<String> records) {
		this.records = records;
	}

	public void add(String line) {
		records.add(line);
		numOfRecords++;
	}

	public boolean empty() {
		return records.size() == 0;
	}
}
